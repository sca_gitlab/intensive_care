from django.apps import AppConfig


class IntensiveCareConfig(AppConfig):
    name = 'intensive_care'
