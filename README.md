# intensive_care

[ENG]
swe project for exam - Università degli studi di Verona, Bioinformatics

[ITA]
progetto terapia intensiva per esame di ing sw - Università degli studi di Verona, Bioinformatica

**Ingegneria del Software 2018-2019**

**Esercizio 1**

Si vuole progettare un sistema informatico per gestire l’acquisizione di dati e segnali in una divisione di terapia
intensiva.

Per ogni paziente ricoverato sono registrati i dati anagrafici principali: codice sanitario univoco, cognome, nome,
data e luogo di nascita. Al momento del ricovero in terapia intensiva è registrata la diagnosi di ingresso di ogni
paziente.

Sono poi gestiti durante il ricovero i dati relativi alle prescrizioni di farmaci e alla loro somministrazione. Per ogni
prescrizione il sistema memorizza il farmaco prescritto, identificato dal suo nome commerciale, la data della
prescrizione, la durata della terapia, il numero di dosi giornaliere e la quantità di farmaco per ogni dose. Viene
inoltre memorizzato il medico responsabile della prescrizione.

Quando il personale infermieristico somministra i farmaci in accordo con le varie prescrizioni, inserisce data e
momento della somministrazione (fino ai minuti), dose somministrata ed eventuali note sullo stato del paziente.
Il sistema da progettare riceve inoltre, dal sistema di monitoraggio dei parametri vitali, differenti segnali registrati a
differenti frequenze: le pressioni sistolica e diastolica (SBP e DBP) ogni 2 minuti; la frequenza cardiaca ogni 5
minuti; la temperatura ogni 3 minuti, eventuali allarmi relativi all’andamento dei parametri vitali e
dell’elettrocardiogramma. Tali allarmi hanno tre diversi livelli di gravità crescente e sono distinti in: aritmia (livello 1),
tachicardia (livello 1), flutter o fibrillazione ventricolare (livello 3), ipertensione (livello 2), ipotensione (livello 2),
ipertermia (livello 2), ipotermia (livello 2).

Il sistema permette a medici e infermieri di inserire i dati di loro pertinenza e di osservare i dati relativi al
monitoraggio del paziente nelle ultime due ore. Se ci sono allarmi di livello 3, i medici devono “spegnere l’allarme”
entro 1 minuto e indicare le attività effettuate sul paziente per riportarlo ad uno stato normale. Lo stesso avviene
per allarmi di livello 1 o 2, ma con tempi di reazioni di 3 e 2 minuti, rispettivamente.

Quando il paziente viene ricoverato, il personale infermieristico inserisce i principali dati anagrafici. Il medico
completa poi tali dati con la diagnosi di ingresso. Durante il ricovero i medici inseriscono i dati delle prescrizioni, gli
infermieri quelle relative alle somministrazioni. Entrambi possono visualizzare per tutti i pazienti ricoverati (al
massimo 10) i parametri monitorati delle ultime due ore e le somministrazioni degli ultimi due giorni. I parametri
vitali rilevati vengono aggiornati in tempo reale anche nella visualizzazione.

Medici e infermieri accedono al sistema previa registrazione. Il sistema di visualizzazione dei parametri vitali per gli
ultimi 15 minuti è sempre in funzione, indipendentemente dal fatto che qualche utente si sia autenticato.
Il primario della divisione può visualizzare e stampare un report riassuntivo della situazione di tutti i pazienti
ricoverati per ogni settimana.

Al termine del ricovero di un paziente, il primario compila una lettera di dimissioni riassuntiva di quanto avvenuto al
paziente durante il ricovero e chiude in tal modo la cartella clinica del ricovero.

Il sistema permette di consultare le cartelle dei ricoveri pregressi e di monitorare i parametri e le informazioni dei
pazienti correntemente ricoverati.